#include "mbed.h"
 
Serial pc(SERIAL_TX, SERIAL_RX);

I2C i2c(D14, D15); // SDA, SCL
DigitalOut led(D13);
 
int main()
{
    i2c.frequency(100000);
    while (1) {
        pc.printf("Searching for I2C devices...\n\r");
 
        int count = 0;
        for (int address = 0; address < 255; address +=2) { // check only for device's read addres
            if (!i2c.write(address, NULL, 0)) { // 0 returned is ok
                pc.printf("I2C device found at address 0x%02X (0x%02X in 8-bit)\r\n", address >> 1, address);  // the address is without LSB, which is R/W flag. shoft it right once
                count++;
                led = 1;
            }
            wait(0.02);
        }
        if (count)
            pc.printf("%d", count);
        else
            pc.printf("No");
        pc.printf(" device%c found\n\r\n", count == 1?'\0':'s');
        led = 0;
        wait (1.8);
    }
}