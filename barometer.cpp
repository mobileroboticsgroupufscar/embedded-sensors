#include "barometer.h"
#include "mbed.h"


Barometer::Barometer(PinName pSDA, PinName pSCL, uint32_t _f):_i2c(pSDA, pSCL)
{
    _oss=0;
    _temp=0;
    _pressure=0;
    _i2c.frequency(_f);
 
 }
 
 int16_t Barometer::checkConnection(void)
 {
     wait_ms(20);
     int _sensorStatus;
     _sensorStatus = _i2c.write(baro_dev_write, NULL, 0);     
    return _sensorStatus;     
}

int16_t Barometer::getCalibrationParameters(void)
{
    int _sensorStatus=0; //0 é ok, i+1 é erro na leitura i.
    char rReg[2] = {0,0};
    char calAddr = AC1_MSB;
    for (int i = 0; i < 22; i++) 
    {
        _i2c.write(baro_dev_write, &calAddr, 1);
        _i2c.read(baro_dev_read,rReg,1);
        calData[i] =  rReg[0];
        calAddr += 1;
        wait_ms(10);
    }
    for(int i=1; i < 22; i+=2)
    {
        int16_t test = calData[i-1] << 8 | calData[i];
        if(test == 0 || test == 0xFF)
        {
            _sensorStatus += i+1;
        }        
    }
    _ac1= (int16_t)(calData[0] << 8 | calData[1]);
    _ac2= (int16_t)(calData[2] << 8 | calData[3]);
    _ac3= (int16_t)(calData[4] << 8 | calData[5]);
    _ac4= (uint16_t)(calData[6] << 8 | calData[7]);
    _ac5= (uint16_t)(calData[8] << 8 | calData[9]);
    _ac6= (uint16_t)(calData[10] << 8 | calData[11]);
    _b1= (int16_t)(calData[12] << 8 | calData[13]);
    _b2= (int16_t)(calData[14] << 8 | calData[15]);
    _mb= (int16_t)(calData[16] << 8 | calData[17]);
    _mc= (int16_t)(calData[18] << 8 | calData[19]);
    _md= (int16_t)(calData[20] << 8 | calData[21]);
       
    return _sensorStatus;
}

char Barometer::debugParamenters(int i)
{
    return calData[i];
}


int16_t Barometer::requestTemperature(void)
{
    char wReg[2] = {request_temp_reg,request_temp_value};
    
    _i2c.write(baro_dev_write,wReg,2);
    
    return 0;
}
    
    
int64_t Barometer::getTemperature(void)
{
    char cmd = temp_MSB;
    char rReg[2] = {0,0};
    _i2c.write(baro_dev_write, &cmd, 1);
    _i2c.read(baro_dev_write,rReg,2);
    int64_t ut = (rReg[0] << 8) | rReg[1];
    _temp = calculateTemp(ut);
    return _temp;    
}

int64_t Barometer::calculateTemp(int64_t ut) 
{
    int64_t _t, x1,x2, b5;
 
    x1 = ((ut - (int64_t) _ac6) * (int64_t) _ac5) >> 15;
    x2 = ((int64_t) _mc << 11) / (x1 + _md);
    b5 = x1 + x2;
    _t = ((b5 + 8) >> 4);  // temperature in 0.1°C
 
    return (_t);
}


