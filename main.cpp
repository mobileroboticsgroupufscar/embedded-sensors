#include "mbed.h"
#include "barometer.h"


Serial pc(SERIAL_TX, SERIAL_RX);

//I2C i2c(D14, D15); // SDA, SCL
DigitalOut led(D13);
Barometer p_t(D14, D15, 100000);
DigitalOut timeTester(D8);
InterruptIn button(USER_BUTTON);

bool _btnFunction=false;
bool isReady = false;
int sensorStatus = 0;
int64_t temperature = 0;
 
 void pressed()
{
    _btnFunction = !_btnFunction;
}

void wait_user()
{
    while(!_btnFunction) {
        pc.printf("waiting... \r\n");
        wait_ms(10);
    }
    _btnFunction = 0;
}
 
 
 
int main()
{
    button.fall(&pressed);
    timeTester = 0;
    led = 1;
    wait_user();
    led = 0;
    isReady = p_t.checkConnection();
    if (!isReady)
    { // 0 returned is ok
        pc.printf("Leu o barometro\r\n");  // the address is without LSB, which is R/W flag. shoft it right once
        led = 1;
        sensorStatus = p_t.getCalibrationParameters();  
    }
    else
    {
        pc.printf("deu errado!\r\n");
        wait_user();
    }
    led = 0;
    while (1) 
    {
        led=1;
        p_t.requestTemperature();
        wait_ms(5);
        temperature = p_t.getTemperature();
        led = 0;
        pc.printf("temperature is [%ld] \r\n", (long)temperature);
        

       wait(1);
    }
}