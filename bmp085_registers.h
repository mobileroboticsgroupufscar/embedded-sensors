// Registers for BMP085

#define baro_dev_write 0xEE
#define baro_dev_read 0xEF

//Calibration Terms
#define AC1_MSB 0xAA
#define AC2_MSB 0xAC
#define AC3_MSB 0xAE
#define AC1_LSB 0xAB
#define AC2_LSB 0xAD
#define AC3_LSB 0xAF
#define AC4_MSB 0xB0
#define AC5_MSB 0xB2
#define AC6_MSB 0xB4
#define AC4_LSB 0xB1
#define AC5_LSB 0xB3
#define AC6_LSB 0xB5
#define B1_MSB 0xB6
#define B2_MSB 0xB8
#define MB_MSB 0xBA
#define MC_MSB 0xBC
#define MD_MSB 0xBE
#define B1_LSB 0xB7
#define B2_LSB 0xB9
#define MB_LSB 0xBB
#define MC_LSB 0xBD
#define MD_LSB 0xBF


//Temperature sensor
#define request_temp_value 0x2E
#define request_temp_reg 0xF4
#define temp_MSB 0xF6
#define temp_LSB 0xF7

//Pressure Sensor
#define request_P_value_0_oss 0x34
#define request_P_value_1_oss 0x74
#define request_P_value_2_oss 0xB4
#define request_P_value_3_oss 0xF4
#define request_P_register 0xF4
#define pres_MSB 0xF6
#define pres_LSB 0xF7
#define pres_xLSB 0xF8





