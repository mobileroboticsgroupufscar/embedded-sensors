#ifndef MBED_BAROMETER_H
#define MBED_BAROMETER_H

#include "mbed.h"
#include "bmp085_registers.h"

/** Barometer class, written for BMP085
 *
 */
class Barometer {

public:
    /** Create a servo object connected to the specified I2C pins
     *
     * @param SDA and SCL I2C pins to connect to 
     */
     Barometer(PinName pSDA, PinName pSCL, uint32_t _f);

    int16_t checkConnection(void);
    int16_t getCalibrationParameters(void);
    int16_t requestTemperature(void);
    int16_t requestPressure(int16_t oversampling);
    int64_t getTemperature(void);

    int64_t getPressure(void);
    
    char debugParamenters(int i);
    
    
    
protected:
    I2C _i2c;
    int16_t _oss;
    int16_t _ac1;
    int16_t _ac2;
    int16_t _ac3;
    uint16_t _ac4;
    uint16_t _ac5;
    uint16_t _ac6;
    int16_t _b1;
    int16_t _b2;
    int16_t _mb;
    int16_t _mc;
    int16_t _md;
    char calData[22];
    int64_t _temp;
    int64_t _pressure;
       
    int64_t calculateTemp(int64_t ut);
};

#endif
